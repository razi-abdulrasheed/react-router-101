## Installing and Running

To start, make sure you're in the `react-router-v4-101` folder in command-line.

```sh
# Install Node Modules
npm install

# If you want to start the server and edit the react code, this rebuilds
npm run dev
```

> Refer terminal to get server port.
