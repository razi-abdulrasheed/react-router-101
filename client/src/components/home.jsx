import React from 'react';
import UserList from './user-list'

class Home extends React.Component {
  render () {
    return (
      <div className="home-page">
        <h1>The app has React Only</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste totam, cum adipisci error porro illo repellendus atque, natus, at minus expedita? Facere sit ullam eos saepe aperiam perspiciatis cumque cum?
       </p>
        <UserList />
      </div>
    );
  }
};

export default Home;