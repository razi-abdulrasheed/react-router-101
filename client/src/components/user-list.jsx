import React from 'react';
import { Link } from 'react-router';

class UserList extends React.Component{
  render () {
    return (
      <ul className="user-list">
        <li>MUKUND</li>
        <li>AKSHAY</li>
        <li>Jeeva</li>
        <li>Akshay</li>
        <li>Jyothi</li>
        <li>Shweta</li>
        <li>Neel</li>
        <li>Dattu</li>
        <li>John</li>
        <li>KK</li>
        <li>Razi</li>
      </ul>
    );
  }

}

export default UserList;